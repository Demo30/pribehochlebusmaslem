﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChlebaSMaslem.ChlebaSMaslemCore
{
    public class VesnickyStaresina
    {
        public string TellTheTale(Pecivo[] protagoniste)
        {
            StringBuilder sb = new StringBuilder();

            List<Pecivo> whoWeMet = new List<Pecivo>();
            whoWeMet.Add(protagoniste[0]);
            //int counter = 0;

            for (int i = 1; i < protagoniste.Length; i++)
            {
                Pecivo newEncounter = protagoniste[i];

                string uvod = i == 1 ? "Jde" : " Takže jde";
                string pronoun_7 = i == 1 ? "s tebou" : "s vámi";
                string meetsVerb = i == 1 ? "potká" : "potkají";

                string s = String.Format(
                    "{0} {1} a {7} {2}. {3} povídá: \"{4}, můžu jít {5}?\", " +
                    "přičemž {6} odpoví: \"Jo, můžeš.\"",
                        uvod,
                        this.GetNames(whoWeMet.ToArray(), 1),
                        this.GetDeclination(newEncounter, 4),
                        this.UppercaseFirst(this.GetDeclination(newEncounter, 1)),
                        this.GetNames(whoWeMet.ToArray(), 5),
                        pronoun_7,
                        this.GetNames(whoWeMet.ToArray(), 1),
                        meetsVerb
                );
                sb.Append(s);

                whoWeMet.Add(newEncounter);
            }

            return sb.ToString();
        }

        private string GetNames(Pecivo[] pastry, int declination)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < pastry.Length; i++)
            {
                if (i > 0 && i < pastry.Length - 1) { sb.Append(", "); }
                else if (i > 0 && i == pastry.Length - 1) { sb.Append(" a "); }

                sb.Append(this.GetDeclination(pastry[i], declination));
            }

            return sb.ToString();
        }

        private string GetDeclination(Pecivo pastry, int declination)
        {
            switch(declination)
            {
                case 1: return pastry.Declination_1;
                case 2: return pastry.Declination_2;
                case 3: return pastry.Declination_3;
                case 4: return pastry.Declination_4;
                case 5: return pastry.Declination_5;
                case 6: return pastry.Declination_6;
                case 7: return pastry.Declination_7;
                default: throw new InvalidOperationException("Dunno this language!");
            }
        }

        private string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}
