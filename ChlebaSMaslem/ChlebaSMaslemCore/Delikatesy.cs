﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChlebaSMaslem.ChlebaSMaslemCore
{
    public class Delikatesy
    {
        private string[] _toppings = new string[]
        {
            "",
            "s máslem",
            "s máslem a se salámem"
        };

        public Pecivo[] AddTheToppings(Pecivo[] bakedPastry)
        {
            List<Pecivo> preparedStuff = new List<Pecivo>();

            for (int i = 0; i < bakedPastry.Length; i++)
            {
                for (int x = 0; x < this._toppings.Length; x++)
                {
                    Pecivo newPastry = bakedPastry[i];

                    if (!(String.IsNullOrEmpty(this._toppings[x])))
                    {
                        newPastry.Declination_1 = String.Concat(newPastry.Declination_1, " ", this._toppings[x]);
                        newPastry.Declination_2 = String.Concat(newPastry.Declination_2, " ", this._toppings[x]);
                        newPastry.Declination_3 = String.Concat(newPastry.Declination_3, " ", this._toppings[x]);
                        newPastry.Declination_4 = String.Concat(newPastry.Declination_4, " ", this._toppings[x]);
                        newPastry.Declination_5 = String.Concat(newPastry.Declination_5, " ", this._toppings[x]);
                        newPastry.Declination_6 = String.Concat(newPastry.Declination_6, " ", this._toppings[x]);
                        newPastry.Declination_7 = String.Concat(newPastry.Declination_7, " ", this._toppings[x]);
                    }
                    preparedStuff.Add(newPastry);
                }
            }

            return preparedStuff.ToArray<Pecivo>();
        }
    }
}
