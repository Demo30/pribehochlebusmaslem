﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChlebaSMaslem.ChlebaSMaslemCore
{
    public struct Pecivo
    {
        public string Declination_1 { get; set; }
        public string Declination_2 { get; set; }
        public string Declination_3 { get; set; }
        public string Declination_4 { get; set; }
        public string Declination_5 { get; set; }
        public string Declination_6 { get; set; }
        public string Declination_7 { get; set; }

        public bool IsValid()
        {
            if (new[]{
                this.Declination_1,
                this.Declination_2,
                this.Declination_3,
                this.Declination_4,
                this.Declination_5,
                this.Declination_6,
                this.Declination_7
                }.Contains(""))
            {
                return false;
            }

            return true;
        }

    }
}
