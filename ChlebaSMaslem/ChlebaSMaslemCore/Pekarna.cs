﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChlebaSMaslem.ChlebaSMaslemCore
{
    public class Pekarna
    {
        private Pecivo[] _pastry = new Pecivo[]
        {
            new Pecivo() {
                Declination_1 = "chleba",
                Declination_2 = "chlebu",
                Declination_3 = "chlebu",
                Declination_4 = "chleba",
                Declination_5 = "chlebe",
                Declination_6 = "chlebu",
                Declination_7 = "chlebem"
            },
            new Pecivo() {
                Declination_1 = "rohlík",
                Declination_2 = "rohlíku",
                Declination_3 = "rohlíku",
                Declination_4 = "rohlík",
                Declination_5 = "rohlíku",
                Declination_6 = "rohlíku",
                Declination_7 = "rohlíkem"
            },
            new Pecivo() {
                Declination_1 = "houska",
                Declination_2 = "housky",
                Declination_3 = "housce",
                Declination_4 = "housku",
                Declination_5 = "housko",
                Declination_6 = "housce",
                Declination_7 = "houskou"
            },
            new Pecivo() {
                Declination_1 = "bageta",
                Declination_2 = "bagety",
                Declination_3 = "bagetě",
                Declination_4 = "begetu",
                Declination_5 = "bageto",
                Declination_6 = "bagetě",
                Declination_7 = "bagetou"
            },
            new Pecivo() {
                Declination_1 = "bulka",
                Declination_2 = "bulky",
                Declination_3 = "bulce",
                Declination_4 = "bulku",
                Declination_5 = "bulko",
                Declination_6 = "bulce",
                Declination_7 = "bulkou"
            },
            new Pecivo() {
                Declination_1 = "dalamánek",
                Declination_2 = "dalamánku",
                Declination_3 = "dalamánku",
                Declination_4 = "dalamánek",
                Declination_5 = "dalamánku",
                Declination_6 = "dalamánku",
                Declination_7 = "dalamánkem"
            },
            new Pecivo() {
                Declination_1 = "croissant",
                Declination_2 = "croissantu",
                Declination_3 = "croissantu",
                Declination_4 = "croissant",
                Declination_5 = "croissante",
                Declination_6 = "croissantu",
                Declination_7 = "croissantem"
            },
            new Pecivo() {
                Declination_1 = "žemle",
                Declination_2 = "žemle",
                Declination_3 = "žemli",
                Declination_4 = "žemli",
                Declination_5 = "žemle",
                Declination_6 = "žemli",
                Declination_7 = "žemlí"
            }
        };

        public Pecivo[] GetThePastry()
        {
            return this._pastry;
        }
    }
}
