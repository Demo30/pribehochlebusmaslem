﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChlebaSMaslem.ChlebaSMaslemCore;

namespace ChlebaSMaslem
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void btn_Bake_Click(object sender, RoutedEventArgs args)
        {
            Pekarna p = new Pekarna();
            Delikatesy d = new Delikatesy();
            Pecivo[] pecivo = d.AddTheToppings(p.GetThePastry());

            VesnickyStaresina storyTeller = new VesnickyStaresina();
            string theTale = storyTeller.TellTheTale(pecivo);

            this.txtbx_Result.Text = theTale;
        }
    }
}
